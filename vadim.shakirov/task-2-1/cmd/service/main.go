package main

import (
	"fmt"
	"os"
)

type interval struct {
	left  int
	right int
}

func main() {
	var numDepartments int

	fmt.Fscan(os.Stdin, &numDepartments)

	var tempIntervals = make([]interval, numDepartments)

	for dep := 0; dep < numDepartments; dep++ {
		tempIntervals[dep].left = 15
		tempIntervals[dep].right = 30
	}

	for dep := 0; dep < numDepartments; dep++ {
		var numPeople int

		fmt.Fscan(os.Stdin, &numPeople)

		for hum := 0; hum < numPeople; hum++ {
			var borderSide string
			var borderVal int
			fmt.Fscan(os.Stdin, &borderSide)
			fmt.Fscan(os.Stdin, &borderVal)

			if borderSide == ">=" && borderVal > tempIntervals[dep].left {
				tempIntervals[dep].left = borderVal
			} else if borderSide == "<=" && borderVal < tempIntervals[dep].right {
				tempIntervals[dep].right = borderVal
			} else if borderSide != ">=" && borderSide != "<=" {
				fmt.Println("Неверный ввод границы")
			}

			if tempIntervals[dep].right >= tempIntervals[dep].left {
				fmt.Println(tempIntervals[dep].left)
			} else {
				fmt.Println(-1)
			}
		}
		fmt.Println()
	}
}
