package main

import (
	"container/heap"
	"fmt"
	"os"
)

// An IntHeap is a min-heap of ints.
type IntHeap []int

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] > h[j] }
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x any) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() any {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}
func (h *IntHeap) Dump() {
	old := *h
	n := len(old)
	for i := 0; i < n; i++ {
		fmt.Println(old[i])
	}
}

func main() {
	var N int
	fmt.Fscan(os.Stdin, &N)

	dishes := &IntHeap{}
	for i := 0; i < N; i++ {
		var dish int
		fmt.Fscan(os.Stdin, &dish)
		dishes.Push(dish)
	}
	var k int
	fmt.Fscan(os.Stdin, &k)

	if k > N {
		panic("k must be less then N")
	}

	heap.Init(dishes)

	for i := 0; i < k-1; i++ {
		heap.Pop(dishes)
	}
	fmt.Println(heap.Pop(dishes))
}
