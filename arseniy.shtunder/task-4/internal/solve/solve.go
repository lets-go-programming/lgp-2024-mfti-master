package solve

import "errors"

func Solve(num1, num2 float64, operator string) (float64, error) {
    res := 0.0
    switch operator {
        case "+":
            res = num1 + num2
        case "-":
            res = num1 - num2
        case "*":
            res = num1 * num2
        case "/":
            if (num2 == 0.0) {
                return 0.0, errors.New("error: Dividing by zero")
            }
            res = num1 / num2
        default:
            return 0.0, errors.New("error: Invalid operator")
    }
    return res, nil
}